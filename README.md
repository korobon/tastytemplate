Basic Project Template with Bootstrap-Sass
============================

DIRECTORY STRUCTURE
-------------------

      bower_components/             contains packges from components added
      css/                          contains files css compiled
      fonts/                        contains fonts
      image/                        contains all images
      js/                           contains app js
      sass/                         contains app.scss with import file for compilation sass & files custom class and var


INSTALLATION
------------

### Install node

Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine. Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient. Node.js' package ecosystem, npm, is the largest ecosystem of open source libraries in the world. (https://nodejs.org/en/)

### Install koala

Koala is a GUI application for Less, Sass, Compass and CoffeeScript compilation, to help web developers to use them more efficiently. Koala can run in windows, linux and mac. (http://koala-app.com/)

### Install bower

Bower can manage components that contain HTML, CSS, JavaScript, fonts or even image files. Bower doesn’t concatenate or minify code or do anything else - it just installs the right versions of the packages you need and their dependencies. (http://koala-app.com/)

cd project/
npm install -g bower

### Install Bootstrap-Sass

bootstrap-sass is a Sass-powered version of Bootstrap 3, ready to drop right into your Sass powered applications. (https://www.npmjs.com/package/bootstrap-sass)

cd project/
bower install bootstrap-sass --save

add file sass/app.scss

add to file app.scss
@import "../bower_components/bootstrap-sass/assets/stylesheets/bootstrap";
@import "../bower_components/bootstrap-sass/assets/stylesheets/bootstrap-compass";

### Install font-awesome

cd project/
bower install fontawesome --save

add to file sass/app.scss
@import "../bower_components/font-awesome/scss/font-awesome";

copy files
project/bower_components/font-awesome/fonts  in project/fonts

### Install Live Server

This is a little development server with live reload capability. Use it for hacking your HTML/JavaScript/CSS files, but not for deploying the final site. (https://github.com/tapio/live-server)

cd project/
npm install -g live-server
live-server --port=8000



RUN
------------
http://localhost/project

or with live-server

http://localhost:8000


**NOTES:**
- Guide documented in tutor Custom Bootstrap Theme With Sass (https://www.youtube.com/watch?v=pB7EwxwSfVk)
